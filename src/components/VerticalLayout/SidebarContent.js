import React, { useEffect } from "react";

// MetisMenu
import MetisMenu from "metismenujs";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

//i18n
import { withNamespaces } from "react-i18next";

const SidebarContent = (props) => {
  // Use ComponentDidMount and ComponentDidUpdate method symultaniously
  useEffect(() => {
    var pathName = props.location.pathname;

    const initMenu = () => {
      new MetisMenu("#side-menu");
      var matchingMenuItem = null;
      var ul = document.getElementById("side-menu");
      var items = ul.getElementsByTagName("a");
      for (var i = 0; i < items.length; ++i) {
        if (pathName === items[i].pathname) {
          matchingMenuItem = items[i];
          break;
        }
      }
      if (matchingMenuItem) {
        activateParentDropdown(matchingMenuItem);
      }
    };
    initMenu();
  }, [props.location.pathname]);

  function activateParentDropdown(item) {
    item.classList.add("active");
    const parent = item.parentElement;

    if (parent) {
      parent.classList.add("mm-active");
      const parent2 = parent.parentElement;

      if (parent2) {
        parent2.classList.add("mm-show");

        const parent3 = parent2.parentElement;

        if (parent3) {
          parent3.classList.add("mm-active"); // li
          parent3.childNodes[0].classList.add("mm-active"); //a
          const parent4 = parent3.parentElement;
          if (parent4) {
            parent4.classList.add("mm-active");
          }
        }
      }
      return false;
    }
    return false;
  }

  return (
    <React.Fragment>
      <div id="sidebar-menu">
        <ul className="metismenu list-unstyled" id="side-menu">
          <li className="menu-title">{props.t("Menu")} </li>
          <li>
            <Link to="/#" className="waves-effect">
              <i className="bx bx-home-circle"></i>
              <span>{props.t("Main Component")}</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to="/home">{props.t("Home")}</Link>
              </li>
              <li>
                <Link to="/home-card">{props.t("Home Card")}</Link>
              </li>
              <li>
                <Link to="/basic-info">{props.t("Basic Info")}</Link>
              </li>
              <li>
                <Link to="/info-kondisi-kulit">{props.t("Info Kondisi Kulit")}</Link>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/user-management" className="waves-effect">
              <i className="bx bx-user-circle"></i>
              <span>{props.t("Users Management")}</span>
            </Link>
          </li>
          <li>
            <Link to="/#" className="waves-effect">
              <i className="bx bx-list-ul"></i>
              <span>{props.t("Kondisi Kulit Management")}</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to="/kondisi-kulit">{props.t("Kondisi Kulit")}</Link>
              </li>
              <li>
                <Link to="/alergi-kulit">{props.t("Alergi Kulit")}</Link>
              </li>
              <li>
                <Link to="/masalah-kulit">{props.t("Masalah Kulit")}</Link>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/#" className="waves-effect">
              <i className="bx bx-list-ol"></i>
              <span>{props.t("Product List")}</span>
            </Link>
            <ul className="sub-menu" aria-expanded="false">
              <li>
                <Link to="/all-product">{props.t("All Product")}</Link>
              </li>
              <li>
                <Link to="/alergi-product">{props.t("Alergi Product")}</Link>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
};

export default withRouter(withNamespaces()(SidebarContent));
