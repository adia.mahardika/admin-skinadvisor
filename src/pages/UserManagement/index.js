import React, { useState, useEffect } from "react";
import {
  Container,
  Card,
  CardBody,
  CardTitle,
  Modal,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
} from "reactstrap";
import { readUser, deleteUser } from "../../store/pages/user/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CSVLink } from "react-csv";
import { useHistory } from "react-router-dom";

import Breadcrumbs from "../../components/Common/Breadcrumb";
import "../../assets/css/input.css";
const User = (props) => {
  const user = props.user;
  const total_pages = props.total_pages;
  const next_pages = props.next_page;
  const previous_page = props.previous_page;
  const history = useHistory();
  console.log(user)

  const headers = [
    { label: "Nama User", key: "name" },
    { label: "Tanggal Lahir", key: "birth_date" },
    { label: "Jenis Kelamin", key: "gender" },
    { label: "Kondisi Kulit", key: "kondisi_kulit" },
    { label: "Alergi Kulit", key: "alergi_kulit" },
    {
      label: "Kandungan Lainnya Pada Alergi Kulit",
      key: "alergi_kulit_user_description",
    },
    { label: "Masalah Kulit", key: "masalah_kulit" },
    { label: "Tanggal Menggunakan Skinadvisor", key: "date_created" },
  ];

  const data = user || "";
  const [modalDetail, setDetail] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [date, setDate] = useState("");
  const [search, setSearch] = useState("");
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);
  let today = new Date().toISOString().substr(0, 10);
  const removeBodyCss = () => {
    document.body.classList.add("no_padding");
  };

  const onChangeDate = (event) => {
    setDate({
      ...date,
      [event.target.name]: event.target.value,
    });
  };
  const onSearch = (event) => {
    props.readUser(date, event.target.value);
    setSearch(event.target.value);
    history.push("/user-management");
  };
  useEffect(() => {
    props.readUser(date, search, limit, page);
  }, []);

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs
            title={"User Management"}
            breadcrumbItem={"User Management"}
          />
          <Card>
            <CardBody>
              <CardTitle>User Management</CardTitle>
              <div
                style={{
                  display: "grid",
                  gridAutoFlow: "column",
                  gridAutoColumns: "max-content",
                  columnGap: "8px",
                  justifyContent: "flex-end",
                }}
              >
                <input
                  placeholder="Search User"
                  onChange={onSearch}
                  style={{
                    borderRadius: "25px",
                    padding: "4px",
                    borderWidth: "1px",
                    borderColor: "gray",
                    borderStyle: "solid",
                    width: "250px",
                    height: "max-content",
                    alignSelf: "flex-end",
                    marginBottom: "1rem",
                  }}
                />
                <div className="form-group" style={{ display: "grid" }}>
                  <label
                    htmlFor="example-date-input"
                    className="col-form-label"
                    style={{ width: "max-content" }}
                  >
                    Start Date
                  </label>
                  <div>
                    <input
                      id="example-date-input"
                      type="date"
                      className="form-control text-align-center"
                      name="start"
                      max={today}
                      onChange={onChangeDate}
                    />
                  </div>
                </div>
                <div className="form-group" style={{ display: "grid" }}>
                  <label
                    htmlFor="example-date-input"
                    className="col-form-label"
                    style={{ width: "max-content" }}
                  >
                    End Date
                  </label>
                  <div>
                    <input
                      className="form-control"
                      type="date"
                      id="example-date-input"
                      name="end"
                      max={today}
                      min={date && date.start}
                      onChange={onChangeDate}
                    />
                  </div>
                </div>
                <button
                  type="button"
                  className="btn btn-primary waves-effect waves-light"
                  style={{
                    width: "max-content",
                    height: "max-content",
                    alignSelf: "flex-end",
                    marginBottom: "1rem",
                  }}
                  onClick={() => props.readUser(date, search)}
                >
                  <i className="bx bx-filter-alt font-size-16 align-middle mr-2"></i>{" "}
                  Filter
                </button>
                <CSVLink
                  data={data}
                  headers={headers}
                  style={{
                    width: "max-content",
                    height: "max-content",
                    alignSelf: "flex-end",
                    marginBottom: "1rem",
                  }}
                  separator={";"}
                  filename={"skinadvisor-users.csv"}
                >
                  <button
                    type="button"
                    className="btn btn-success waves-effect waves-light"
                  >
                    <i className="bx bxs-file-export font-size-16 align-middle mr-2"></i>{" "}
                    Export
                  </button>
                </CSVLink>
              </div>
              <div className="table-responsive">
                <Table className="table table-centered">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Name</th>
                      <th scope="col">Birth Date</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Skinadvisor Using Date</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {user &&
                      user.map((value, index) => {
                        return (
                          <tr key={value.id}>
                            <th scope="row">
                              <div>{index + 1}</div>
                            </th>
                            <td>{value.name}</td>
                            <td>{value.birth_date}</td>
                            <td>{value.gender}</td>
                            <td>{value.date_created}</td>
                            <td>
                              <div
                                style={{
                                  display: "grid",
                                  rowGap: "8px",
                                }}
                              >
                                <button
                                  type="button"
                                  className="btn btn-info waves-effect waves-light"
                                  style={{ minWidth: "max-content" }}
                                  onClick={() => {
                                    setDetail(!modalDetail);
                                    setSelectedData(value);
                                  }}
                                >
                                  <i className="bx bx-show-alt font-size-16 align-middle mr-2"></i>{" "}
                                  Detail
                                </button>
                                <button
                                  type="button"
                                  className="btn btn-danger waves-effect waves-light"
                                  style={{ minWidth: "max-content" }}
                                  onClick={() => {
                                    setModalDelete(!modalDelete);
                                    setSelectedData(value);
                                  }}
                                >
                                  <i className="bx bx-trash font-size-16 align-middle mr-2"></i>{" "}
                                  Delete
                                </button>
                              </div>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </Table>
                {user && user.length <= 0 && (
                  <div style={{ textAlign: "center" }}>No Data</div>
                )}
              </div>
              <div
                style={{
                  display: "grid",
                  gridAutoFlow: "column",
                  justifyContent: "flex-end",
                }}
              >
                <Pagination aria-label="Page navigation example">
                  <PaginationItem disabled>
                    <PaginationLink href="#" tabIndex="-1">
                      Previous
                    </PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink href="#">1</PaginationLink>
                  </PaginationItem>
                  <PaginationItem active>
                    <PaginationLink href="#">
                      2 <span className="sr-only">(current)</span>
                    </PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink href="#">3</PaginationLink>
                  </PaginationItem>
                  <PaginationItem>
                    <PaginationLink href="#">Next</PaginationLink>
                  </PaginationItem>
                </Pagination>
              </div>
            </CardBody>
          </Card>

          {/* Modal Detail */}
          <Modal
            isOpen={modalDetail}
            toggle={() => {
              setDetail(!modalDetail);
              removeBodyCss();
            }}
          >
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Detail User
              </h5>
              <button
                type="button"
                onClick={() => {
                  setDetail(false);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <h5>User Profile</h5>
              <div
                className="form-group"
                style={{
                  display: "grid",
                  gridTemplateColumns: "1fr repeat(2, max-content)",
                  columnGap: "8px",
                }}
              >
                <div
                  style={{
                    alignItems: "center",
                    display: "flex",
                  }}
                >
                  <div>
                    <strong>Name :</strong>
                  </div>
                  &ensp;
                  <div>{selectedData && selectedData.name}</div>
                </div>
                <div style={{ alignItems: "center", display: "flex" }}>
                  <div>
                    <strong>Birth Date :</strong>
                  </div>
                  &ensp;
                  <div>{selectedData && selectedData.birth_date}</div>
                </div>
                <div style={{ alignItems: "center", display: "flex" }}>
                  <div>
                    <strong>Gender :</strong>
                  </div>
                  &ensp;
                  <div>{selectedData && selectedData.gender}</div>
                </div>
              </div>
              <h5>Kondisi Kulit User</h5>
              <div
                className="form-group"
                style={{
                  display: "grid",
                  gridTemplateColumns: "repeat(3, 1fr)",
                  columnGap: "8px",
                }}
              >
                <div className="form-group" style={{ alignItems: "center" }}>
                  <div>
                    <strong>Kondisi Kulit :</strong>
                  </div>
                  <div>
                    {selectedData &&
                      selectedData.kondisi_kulit.map((value, index) => {
                        return (
                          <div>
                            {index + 1}.&ensp;{value}
                          </div>
                        );
                      })}
                  </div>
                </div>
                <div className="form-group" style={{ alignItems: "center" }}>
                  <div>
                    <strong>Alergi Kulit :</strong>
                  </div>
                  <div>
                    {selectedData &&
                      selectedData.alergi_kulit.map((value, index) => {
                        return (
                          <div>
                            {index + 1}.&ensp;{value}
                          </div>
                        );
                      })}
                  </div>
                </div>
                <div className="form-group" style={{ alignItems: "center" }}>
                  <div>
                    <strong>Masalah Kulit :</strong>
                  </div>
                  <div>
                    {selectedData &&
                      selectedData.masalah_kulit.map((value, index) => {
                        return (
                          <div>
                            {index + 1}.&ensp;{value}
                          </div>
                        );
                      })}
                  </div>
                </div>
              </div>
              <div
                className="form-group"
                style={{ alignItems: "center", display: "flex" }}
              >
                <div>
                  <strong>Alergi Kulit Description :</strong>
                </div>
                &ensp;
                <div>
                  {selectedData && selectedData.alergi_kulit_user_description}
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setDetail(!modalDetail);
                  removeBodyCss();
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </Modal>

          {/* Modal Delete */}
          <Modal
            isOpen={modalDelete}
            toggle={() => {
              setModalDelete(!modalDelete);
              removeBodyCss();
            }}
          >
            <div className="modal-header">
              <h5 className="modal-title mt-0" id="myModalLabel">
                Delete Alergi Kulit
              </h5>
              <button
                type="button"
                onClick={() => {
                  setModalDelete(false);
                }}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              Are you sure want to delete this user?
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={() => {
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                }}
                className="btn btn-secondary waves-effect"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-danger waves-effect waves-light"
                onClick={() => {
                  props.deleteUser(selectedData.id);
                  setModalDelete(!modalDelete);
                  removeBodyCss();
                  setSelectedData(null);
                }}
              >
                Delete
              </button>
            </div>
          </Modal>
        </Container>
      </div>
    </React.Fragment>
  );
};

const mapStatetoProps = (state) => {
  const { user, message, loading } = state.User;
  return { user, message, loading };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      readUser,
      deleteUser,
    },
    dispatch
  );

export default connect(mapStatetoProps, mapDispatchToProps)(User);
