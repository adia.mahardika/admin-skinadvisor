// @flow
import {
  CREATE_ALERGI_KULIT,
  CREATE_ALERGI_KULIT_REJECT,
  CREATE_ALERGI_KULIT_FULFILLED,
  READ_ALERGI_KULIT,
  READ_ALERGI_KULIT_REJECT,
  READ_ALERGI_KULIT_FULFILLED,
  UPDATE_ALERGI_KULIT,
  UPDATE_ALERGI_KULIT_REJECT,
  UPDATE_ALERGI_KULIT_FULFILLED,
  DELETE_ALERGI_KULIT,
  DELETE_ALERGI_KULIT_REJECT,
  DELETE_ALERGI_KULIT_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  alergi_kulit: null,
  message: null,
  loading: false,
};

const AlergiKulit = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_ALERGI_KULIT:
      return {
        ...state,
        lading: true,
      };
    case READ_ALERGI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        lading: true,
      };
    case READ_ALERGI_KULIT_FULFILLED:
      return {
        ...state,
        alergi_kulit: action.payload.result,
        lading: false,
      };
    case CREATE_ALERGI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case CREATE_ALERGI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case CREATE_ALERGI_KULIT_FULFILLED:
      return {
        ...state,
        alergi_kulit: action.payload.result,
        loading: false,
      };
    case UPDATE_ALERGI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_ALERGI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case UPDATE_ALERGI_KULIT_FULFILLED:
      return {
        ...state,
        alergi_kulit: action.payload.result,
        lading: false,
      };
    case DELETE_ALERGI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case DELETE_ALERGI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case DELETE_ALERGI_KULIT_FULFILLED:
      return {
        ...state,
        alergi_kulit: action.payload.result,
        loading: false,
      };
    default:
      return state;
  }
};

export default AlergiKulit;
