require("dotenv").config();

export const createAlergiKulitMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/alergi-kulit`, {
    method: "POST",
    mode: "cors",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  });
  return response.json();
};

export const readAlergiKulitMethod = async (search_data) => {
  if (search_data !== "") {
    const response = await fetch(
      `${process.env.REACT_APP_API}/alergi-kulit/?name=${search_data}`,
      {
        method: "GET",
        mode: "cors",
      }
    );
    return response.json();
  } else {
    const response = await fetch(`${process.env.REACT_APP_API}/alergi-kulit`, {
      method: "GET",
      mode: "cors",
    });
    return response.json();
  }
};

export const updateAlergiKulitMethod = async ({ data, id }) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/alergi-kulit/${id}`,
    {
      method: "PATCH",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    }
  );
  return response.json();
};

export const deleteAlergiKulitMethod = async (id) => {
  const response = await fetch(
    `${process.env.REACT_APP_API}/alergi-kulit/${id}`,
    {
      method: "DELETE",
      mode: "cors",
    }
  );
  return response.json();
};
