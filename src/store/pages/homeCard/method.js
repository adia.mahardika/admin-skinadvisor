require("dotenv").config();

export const createHomeCardMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/home-card`, {
    method: "POST",
    mode: "cors",
    body: data,
  });
  return response.json();
};

export const readHomeCardMethod = async () => {
  const response = await fetch(`${process.env.REACT_APP_API}/home-card`, {
    method: "GET",
    mode: "cors",
  });
  return response.json();
};

export const updateHomeCardMethod = async ({ data, id, type }) => {
  if (type === "text") {
    const response = await fetch(`${process.env.REACT_APP_API}/home-card/${id}`, {
      method: "PATCH",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    return response.json();
  } else {
    const response = await fetch(`${process.env.REACT_APP_API}/home-card/${id}`, {
      method: "PATCH",
      mode: "cors",
      body: data,
    });
    return response.json();
  }
};

export const deleteHomeCardMethod = async (id) => {
  const response = await fetch(`${process.env.REACT_APP_API}/home-card/${id}`, {
    method: "DELETE",
    mode: "cors",
  });
  return response.json();
};
