// @flow
import {
  CREATE_KONDISI_KULIT,
  CREATE_KONDISI_KULIT_REJECT,
  CREATE_KONDISI_KULIT_FULFILLED,
  READ_KONDISI_KULIT,
  READ_KONDISI_KULIT_REJECT,
  READ_KONDISI_KULIT_FULFILLED,
  UPDATE_KONDISI_KULIT,
  UPDATE_KONDISI_KULIT_REJECT,
  UPDATE_KONDISI_KULIT_FULFILLED,
  DELETE_KONDISI_KULIT,
  DELETE_KONDISI_KULIT_REJECT,
  DELETE_KONDISI_KULIT_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  kondisi_kulit: null,
  message: null,
  loading: false,
};

const KondisiKulit = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_KONDISI_KULIT:
      return {
        ...state,
        lading: true,
      };
    case READ_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        lading: true,
      };
    case READ_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        kondisi_kulit: action.payload.result,
        lading: false,
      };
    case CREATE_KONDISI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case CREATE_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case CREATE_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        kondisi_kulit: action.payload.result,
        loading: false,
      };
    case UPDATE_KONDISI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case UPDATE_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        kondisi_kulit: action.payload.result,
        lading: false,
      };
    case DELETE_KONDISI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case DELETE_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case DELETE_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        kondisi_kulit: action.payload.result,
        loading: false,
      };
    default:
      return state;
  }
};

export default KondisiKulit;
