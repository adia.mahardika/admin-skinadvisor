require("dotenv").config();

export const readProductMethod = async () => {
  const response = await fetch(`https://www.avoskinbeauty.com/wp-json/avoskin/v1/get_product_list?total=100`, {
    method: "GET",
    mode: "cors",
  });
  return response.json();
};