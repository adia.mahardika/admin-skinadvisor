import {
  READ_USER,
  READ_USER_REJECT,
  READ_USER_FULFILLED,
  DELETE_USER,
  DELETE_USER_REJECT,
  DELETE_USER_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  user: null,
  message: null,
  loading: false,
  total_pages: null,
  next_page: null,
  previous_page: null,
};

const User = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_USER:
      return {
        ...state,
        lading: true,
      };
    case READ_USER_REJECT:
      return {
        ...state,
        message: action.payload.message,
        lading: true,
      };
    case READ_USER_FULFILLED:
      return {
        ...state,
        user: action.payload.result,
        total_pages: action.payload.total_pages,
        next_page: action.payload.next_page,
        previous_page: action.payload.previous_page,
        lading: false,
      };
    case DELETE_USER:
      return {
        ...state,
        loading: true,
      };
    case DELETE_USER_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case DELETE_USER_FULFILLED:
      return {
        ...state,
        user: action.payload.result,
        loading: false,
      };
    default:
      return state;
  }
};

export default User;
