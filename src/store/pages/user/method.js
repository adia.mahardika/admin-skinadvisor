require("dotenv").config();

export const readUserMethod = async ({ date, name, limit, page }) => {
  if (date.start && date.end) {
    const response = await fetch(
      `${process.env.REACT_APP_API}/user?start=${date.start}&end=${date.end}&name=${name}&limit=${limit}&page=${page}`,
      {
        method: "GET",
        mode: "cors",
      }
    );
    return response.json();
  } else {
    const response = await fetch(
      `${process.env.REACT_APP_API}/user?name=${name}&limit=${limit}&page=${page}`,
      {
        method: "GET",
        mode: "cors",
      }
    );
    return response.json();
  }
};

export const deleteUserMethod = async (id) => {
  const response = await fetch(`${process.env.REACT_APP_API}/user/${id}`, {
    method: "DELETE",
    mode: "cors",
  });
  return response.json();
};
