require("dotenv").config();

export const createBasicInfoMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/basic-info`, {
    method: "POST",
    mode: "cors",
    body: data,
  });
  return response.json();
};

export const readBasicInfoMethod = async () => {
  const response = await fetch(`${process.env.REACT_APP_API}/basic-info`, {
    method: "GET",
    mode: "cors",
  });
  return response.json();
};

export const updateBasicInfoMethod = async ({ data, id, type }) => {
  if (type === "text") {
    const response = await fetch(`${process.env.REACT_APP_API}/basic-info/${id}`, {
      method: "PATCH",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    return response.json();
  } else {
    const response = await fetch(`${process.env.REACT_APP_API}/basic-info/${id}`, {
      method: "PATCH",
      mode: "cors",
      body: data,
    });
    return response.json();
  }
};

export const deleteBasicInfoMethod = async (id) => {
  const response = await fetch(`${process.env.REACT_APP_API}/basic-info/${id}`, {
    method: "DELETE",
    mode: "cors",
  });
  return response.json();
};
