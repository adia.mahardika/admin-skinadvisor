import {
  CREATE_INFO_KONDISI_KULIT,
  CREATE_INFO_KONDISI_KULIT_REJECT,
  CREATE_INFO_KONDISI_KULIT_FULFILLED,
  READ_INFO_KONDISI_KULIT,
  READ_INFO_KONDISI_KULIT_REJECT,
  READ_INFO_KONDISI_KULIT_FULFILLED,
  UPDATE_INFO_KONDISI_KULIT,
  UPDATE_INFO_KONDISI_KULIT_REJECT,
  UPDATE_INFO_KONDISI_KULIT_FULFILLED,
  DELETE_INFO_KONDISI_KULIT,
  DELETE_INFO_KONDISI_KULIT_REJECT,
  DELETE_INFO_KONDISI_KULIT_FULFILLED,
} from "./actionTypes";

const INIT_STATE = {
  info_kondisi_kulit: null,
  message: null,
  loading: false,
};

const InfoKondisiKulit = (state = INIT_STATE, action) => {
  switch (action.type) {
    case READ_INFO_KONDISI_KULIT:
      return {
        ...state,
        lading: true,
      };
    case READ_INFO_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        lading: true,
      };
    case READ_INFO_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        info_kondisi_kulit: action.payload.result,
        lading: false,
      };
    case CREATE_INFO_KONDISI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case CREATE_INFO_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case CREATE_INFO_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        info_kondisi_kulit: action.payload.result,
        loading: false,
      };
    case UPDATE_INFO_KONDISI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_INFO_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case UPDATE_INFO_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        info_kondisi_kulit: action.payload.result,
        lading: false,
      };
    case DELETE_INFO_KONDISI_KULIT:
      return {
        ...state,
        loading: true,
      };
    case DELETE_INFO_KONDISI_KULIT_REJECT:
      return {
        ...state,
        message: action.payload.message,
        loading: true,
      };
    case DELETE_INFO_KONDISI_KULIT_FULFILLED:
      return {
        ...state,
        info_kondisi_kulit: action.payload.result,
        loading: false,
      };
    default:
      return state;
  }
};

export default InfoKondisiKulit;
