require("dotenv").config();

export const createInfoKondisiKulitMethod = async (data) => {
  const response = await fetch(`${process.env.REACT_APP_API}/info-kondisi-kulit`, {
    method: "POST",
    mode: "cors",
    body: data,
  });
  return response.json();
};

export const readInfoKondisiKulitMethod = async () => {
  const response = await fetch(`${process.env.REACT_APP_API}/info-kondisi-kulit`, {
    method: "GET",
    mode: "cors",
  });
  return response.json();
};

export const updateInfoKondisiKulitMethod = async ({ data, id }) => {
    const response = await fetch(`${process.env.REACT_APP_API}/info-kondisi-kulit/${id}`, {
      method: "PATCH",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
    return response.json();
};

export const deleteInfoKondisiKulitMethod = async (id) => {
  const response = await fetch(`${process.env.REACT_APP_API}/info-kondisi-kulit/${id}`, {
    method: "DELETE",
    mode: "cors",
  });
  return response.json();
};
